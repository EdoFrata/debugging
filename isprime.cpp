/**
 * isprime.cpp
 *
 * Ask the user for an integer and output whether or not the number
 * provided is a prime number.
 *
 * Created: 15.38pm Monday, 21 September 2020
 * Last updated Time-stamp: <15.52pm Monday, 21 September 2020>
 *
 * Author: Barry D. Nichols <B.Nichols@mdx.ac.uk>
 **/
#include <iostream>

int main(){

  int number;
  std::cout << "Enter an integer: ";
  std::cin >> number;

  // test divisors of number, if a divisor other than 1 and number is
  // found, then number is not prime.
  bool isPrime = true;
  
  if (number == 0 || number == 1) {
        isPrime = false;

  }

  else {

    for (int i = 2; i <= number / 2; ++i) {

      if (number % i == 0) {

	isPrime = false;

	break;

      }

    }

  }
  

  if (isPrime){
      

    std::cout << "Prime\n";


  }

  else{
    

    std::cout << "Not prime\n";

  }
  return 0;
}
